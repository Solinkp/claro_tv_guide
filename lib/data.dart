import 'package:flutter/material.dart';

import './models/segment.dart';
import './models/channel.dart';

const SEGMENTS_DATA = const [
  Segment(id: 's1', title: 'NACIONALES', color: Colors.blueAccent),
  Segment(id: 's2', title: 'FAMILIAR', color: Colors.orange),
  Segment(id: 's3', title: 'INFANTIL', color: Colors.amber),
  Segment(id: 's4', title: 'CULTURAL', color: Colors.lightGreen),///
  Segment(id: 's5', title: 'DEPORTIVA', color: Colors.cyan),
  Segment(id: 's6', title: 'ENTRETENIMIENTO', color: Colors.redAccent),
  Segment(id: 's7', title: 'PELICULAS', color: Colors.pink),
  Segment(id: 's8', title: 'INTERNACIONAL', color: Colors.blue),
  Segment(id: 's9', title: 'NOTICIAS', color: Colors.red),
  Segment(id: 's10', title: 'MUSICAL', color: Colors.purple),
  Segment(id: 's11', title: 'RELIGIOSA', color: Colors.blue),
  Segment(id: 's12', title: 'ADULTOS', color: Colors.blueGrey),
  Segment(id: 's13', title: 'AUDIO DIGITAL', color: Colors.purpleAccent),
];

const CHANNELS_DATA = const [
  Channel(id: 'c1', segmentId: 's1', key: '2', title: 'Canal 2'),
  Channel(id: 'c2', segmentId: 's1', key: '4', title: 'Canal 4'),
  Channel(id: 'c3', segmentId: 's1', key: '5', title: 'Mega Box'),
  Channel(id: 'c4', segmentId: 's1', key: '6', title: 'Canal 6'),
  Channel(id: 'c5', segmentId: 's1', key: '8', title: 'TN8'),
  Channel(id: 'c6', segmentId: 's1', key: '9', title: 'Canal 9'),
  Channel(id: 'c7', segmentId: 's1', key: '10', title: 'Canal 10'),
  Channel(id: 'c8', segmentId: 's1', key: '11', title: 'TV Red'),
  Channel(id: 'c9', segmentId: 's1', key: '12', title: 'Canal 12'),
  Channel(id: 'c10', segmentId: 's1', key: '13', title: 'Viva Nicaragua', hdKey: '99'),
  Channel(id: 'c11', segmentId: 's1', key: '14', title: 'Vos TV'),
  Channel(id: 'c12', segmentId: 's1', key: '15', title: 'Canal 15'),
  Channel(id: 'c13', segmentId: 's1', key: '21', title: 'Canal 21'),
  Channel(id: 'c14', segmentId: 's1', key: '23', title: 'CDNN'),
  Channel(id: 'c15', segmentId: 's1', key: '37', title: 'EXTRA PLUS'),
  Channel(id: 'c16', segmentId: 's1', key: '51', title: 'Canal Católico'),
  Channel(id: 'c17', segmentId: 's1', key: '98', title: 'NTV 98'),

  Channel(id: 'c18', segmentId: 's2', key: '102', title: 'Las Estrellas', hdKey: '1102'),
  Channel(id: 'c19', segmentId: 's2', key: '103', title: 'Sony', hdKey: '1103'),
  Channel(id: 'c20', segmentId: 's2', key: '106', title: 'FOX Latino', hdKey: '1106'),
  Channel(id: 'c21', segmentId: 's2', key: '107', title: 'AZ Mundo'),
  Channel(id: 'c22', segmentId: 's2', key: '108', title: 'AXN', hdKey: '1108'),
  Channel(id: 'c23', segmentId: 's2', key: '110', title: 'Warner Bros', hdKey: '1110'),
  Channel(id: 'c24', segmentId: 's2', key: '112', title: 'Distrito Comedia'),
  Channel(id: 'c25', segmentId: 's2', key: '113', title: 'SyFy'),
  Channel(id: 'c26', segmentId: 's2', key: '114', title: 'Telemundo'),
  Channel(id: 'c27', segmentId: 's2', key: '115', title: 'TBS'),

  Channel(id: 'c28', segmentId: 's3', key: '151', title: 'Discovery Kids', hdKey: '1151'),
  Channel(id: 'c29', segmentId: 's3', key: '152', title: 'Boomerang'),
  Channel(id: 'c30', segmentId: 's3', key: '153', title: 'Nickelodeon', hdKey: '1153'),
  Channel(id: 'c31', segmentId: 's3', title: 'Nickelodeon 2', hdKey: '1161'),
  Channel(id: 'c32', segmentId: 's3', key: '155', title: 'Disney', hdKey: '1155'),
  Channel(id: 'c33', segmentId: 's3', key: '157', title: 'Cartoon Network', hdKey: '1157'),
  Channel(id: 'c34', segmentId: 's3', key: '158', title: 'Disney XD'),
  Channel(id: 'c35', segmentId: 's3', key: '159', title: 'Baby TV'),
  Channel(id: 'c36', segmentId: 's3', key: '160', title: 'Disney Jr.'),
  Channel(id: 'c37', segmentId: 's3', key: '161', title: 'Tiin'),
  Channel(id: 'c38', segmentId: 's3', key: '162', title: 'Nickelodeon Jr.'),
  Channel(id: 'c39', segmentId: 's3', key: '163', title: 'Semillitas'),
  Channel(id: 'c40', segmentId: 's3', key: '164', title: 'Nat Geo Kids'),

  Channel(id: 'c41', segmentId: 's4', key: '170', title: 'Discovery', hdKey: '1170'),
  Channel(id: 'c42', segmentId: 's4', key: '172', title: 'H&H Discovery', hdKey: '1172'),
  Channel(id: 'c43', segmentId: 's4', key: '173', title: 'Nat Geo', hdKey: '1174'),
  Channel(id: 'c44', segmentId: 's4', key: '175', title: 'History', hdKey: '1175'),
  Channel(id: 'c45', segmentId: 's4', key: '189', title: 'History 2'),
  Channel(id: 'c46', segmentId: 's4', key: '177', title: 'TLC'),
  Channel(id: 'c47', segmentId: 's4', key: '179', title: 'A & E', hdKey: '1179'),
  Channel(id: 'c48', segmentId: 's4', key: '181', title: 'Animal Planet', hdKey: '1181'),
  Channel(id: 'c49', segmentId: 's4', key: '183', title: 'Nat Geo Wild', hdKey: '1183'),
  Channel(id: 'c50', segmentId: 's4', key: '185', title: 'Film & Arts'),
  Channel(id: 'c51', segmentId: 's4', key: '186', title: 'Discovery Civilización'),
  Channel(id: 'c52', segmentId: 's4', key: '187', title: 'Discovery Ciencia'),
  Channel(id: 'c53', segmentId: 's4', key: '188', title: 'Discovery Turbo'),
  Channel(id: 'c54', segmentId: 's4', key: '190', title: 'EURO Channel'),
  Channel(id: 'c55', segmentId: 's4', title: 'World Discovery', hdKey: '1191'),
  Channel(id: 'c56', segmentId: 's4', title: 'Theater Discovery', hdKey: '1192'),

  Channel(id: 'c57', segmentId: 's5', key: '201', title: 'Claro Sports', hdKey: '1201'),
  Channel(id: 'c58', segmentId: 's5', key: '203', title: 'ESPN', hdKey: '1203, 1218'),
  Channel(id: 'c59', segmentId: 's5', key: '205', title: 'ESPN 2', hdKey: '1205'),
  Channel(id: 'c60', segmentId: 's5', key: '207', title: 'ESPN 3', hdKey: '1207'),
  Channel(id: 'c61', segmentId: 's5', key: '209', title: 'FOX Sports', hdKey: '1209'),
  Channel(id: 'c62', segmentId: 's5', key: '211', title: 'FOX Sports 2', hdKey: '1211'),
  Channel(id: 'c63', segmentId: 's5', key: '213', title: 'FOX Sports 3', hdKey: '1213'),
  Channel(id: 'c64', segmentId: 's5', key: '215', title: 'TDN'),
  Channel(id: 'c65', segmentId: 's5', key: '217', title: 'Golf Channel'),

  Channel(id: 'c66', segmentId: 's6', key: '250', title: 'Mas Chic'),
  Channel(id: 'c67', segmentId: 's6', key: '252', title: 'RCN Novelas'),
  Channel(id: 'c68', segmentId: 's6', key: '253', title: 'TL Novelas'),
  Channel(id: 'c69', segmentId: 's6', key: '254', title: 'AZ Corazón'),
  Channel(id: 'c70', segmentId: 's6', key: '255', title: 'E!'),
  Channel(id: 'c71', segmentId: 's6', key: '257', title: 'Fox Life'),
  Channel(id: 'c72', segmentId: 's6', key: '262', title: 'Lifetime'),
  Channel(id: 'c73', segmentId: 's6', key: '264', title: 'Space', hdKey: '1322'),
  Channel(id: 'c74', segmentId: 's6', key: '266', title: 'TNT Series'),
  Channel(id: 'c75', segmentId: 's6', key: '267', title: 'ID', hdKey: '1267'),
  Channel(id: 'c76', segmentId: 's6', key: '270', title: 'Venevision TV'),
  Channel(id: 'c77', segmentId: 's6', title: 'Comedy Center', hdKey: '1264'),
  Channel(id: 'c78', segmentId: 's6', title: 'Sundance TV', hdKey: '1273'),
  Channel(id: 'c79', segmentId: 's6', title: '¡Hola! TV', hdKey: '1274'),

  Channel(id: 'c80', segmentId: 's7', key: '301', title: 'HBO'),
  Channel(id: 'c81', segmentId: 's7', key: '302', title: 'HBO Family'),
  Channel(id: 'c82', segmentId: 's7', key: '303', title: 'CINEMAX'),
  Channel(id: 'c83', segmentId: 's7', key: '304', title: 'Cine Canal', hdKey: '1304'),
  Channel(id: 'c84', segmentId: 's7', key: '306', title: 'Universal', hdKey: '1306'),
  Channel(id: 'c85', segmentId: 's7', key: '308', title: 'Studio Universal'),
  Channel(id: 'c86', segmentId: 's7', key: '309', title: 'Golden'),
  Channel(id: 'c87', segmentId: 's7', key: '310', title: 'TNT', hdKey: '1310'),
  Channel(id: 'c88', segmentId: 's7', key: '312', title: 'FX', hdKey: '1312'),
  Channel(id: 'c89', segmentId: 's7', key: '313', title: 'AMC', hdKey: '1318'),
  Channel(id: 'c90', segmentId: 's7', key: '314', title: 'TCM'),
  Channel(id: 'c91', segmentId: 's7', key: '315', title: 'De Película'),
  Channel(id: 'c92', segmentId: 's7', key: '316', title: 'Cine Latino'),
  Channel(id: 'c93', segmentId: 's7', key: '317', title: 'FX Movies'),
  Channel(id: 'c94', segmentId: 's7', key: '321', title: 'Paramount', hdKey: '1321'),
  Channel(id: 'c95', segmentId: 's7', key: '328', title: 'Cinema Dinamita'),
  Channel(id: 'c96', segmentId: 's7', key: '329', title: 'Claro Cine'),

  Channel(id: 'c97', segmentId: 's8', key: '500', title: 'TV5'),
  Channel(id: 'c98', segmentId: 's8', key: '501', title: 'TVE'),
  Channel(id: 'c99', segmentId: 's8', key: '502', title: 'France 24'),
  Channel(id: 'c100', segmentId: 's8', key: '508', title: 'TV Chile'),
  Channel(id: 'c101', segmentId: 's8', key: '509', title: 'Antena 3'),
  Channel(id: 'c102', segmentId: 's8', key: '510', title: 'DW'),
  Channel(id: 'c103', segmentId: 's8', key: '512', title: 'Caracol'),
  Channel(id: 'c104', segmentId: 's8', key: '513', title: 'RT RusiaTv'),
  Channel(id: 'c105', segmentId: 's8', key: '514', title: 'Rai Italia'),
  Channel(id: 'c106', segmentId: 's8', key: '515', title: 'TV Globo'),
  Channel(id: 'c107', segmentId: 's8', key: '516', title: 'ARIRANG'),
  Channel(id: 'c108', segmentId: 's8', key: '519', title: 'CCTV'),
  Channel(id: 'c109', segmentId: 's8', key: '521', title: 'Cuba'),
  Channel(id: 'c110', segmentId: 's8', key: '523', title: 'Telesur'),

  Channel(id: 'c111', segmentId: 's9', key: '400', title: 'CNN Español'),
  Channel(id: 'c112', segmentId: 's9', key: '401', title: 'CNN Internacional', hdKey: '1420'),
  Channel(id: 'c113', segmentId: 's9', key: '403', title: 'FOX News'),
  Channel(id: 'c114', segmentId: 's9', key: '404', title: 'HLN'),
  Channel(id: 'c115', segmentId: 's9', key: '406', title: 'BBC World'),

  Channel(id: 'c116', segmentId: 's10', key: '449', title: 'Claro Música'),
  Channel(id: 'c117', segmentId: 's10', key: '450', title: 'MTV Latino', hdKey: '1451'),
  Channel(id: 'c118', segmentId: 's10', title: 'MTV Live', hdKey: '1452'),
  Channel(id: 'c119', segmentId: 's10', key: '452', title: 'Telehit'),
  Channel(id: 'c120', segmentId: 's10', key: '453', title: 'Ritmoson'),
  Channel(id: 'c121', segmentId: 's10', key: '454', title: 'Bandamax'),
  Channel(id: 'c122', segmentId: 's10', key: '455', title: 'VH-1', hdKey: '1455'),
  Channel(id: 'c123', segmentId: 's10', key: '457', title: 'Conciertos', hdKey: '1457'),
  Channel(id: 'c124', segmentId: 's10', key: '460', title: 'NU Music'),

  Channel(id: 'c125', segmentId: 's11', key: '421', title: 'BYU TV'),
  Channel(id: 'c126', segmentId: 's11', key: '422', title: 'EWTN'),
  Channel(id: 'c127', segmentId: 's11', key: '423', title: 'Enlace'),
  Channel(id: 'c128', segmentId: 's11', key: '426', title: 'ESNE'),
  Channel(id: 'c129', segmentId: 's11', key: '430', title: 'Orbe 21'),

  Channel(id: 'c130', segmentId: 's12', key: '552', title: 'Multipremier'),

  Channel(id: 'c131', segmentId: 's13', key: '601', title: 'Instrumental'),
  Channel(id: 'c132', segmentId: 's13', key: '602', title: 'Opera'),
  Channel(id: 'c133', segmentId: 's13', key: '603', title: 'Groove'),
  Channel(id: 'c134', segmentId: 's13', key: '604', title: 'Classic Jazz'),
  Channel(id: 'c135', segmentId: 's13', key: '605', title: 'Rock Alternativo'),
  Channel(id: 'c136', segmentId: 's13', key: '606', title: '90s Hits'),
  Channel(id: 'c137', segmentId: 's13', key: '607', title: 'Heavy Metal'),
  Channel(id: 'c138', segmentId: 's13', key: '608', title: '70s Hits'),
  Channel(id: 'c139', segmentId: 's13', key: '609', title: '80s Hits'),
  Channel(id: 'c140', segmentId: 's13', key: '610', title: 'Love Music'),
  Channel(id: 'c141', segmentId: 's13', key: '611', title: 'Fiesta Tropical'),
  Channel(id: 'c142', segmentId: 's13', key: '612', title: 'Chicos'),
  Channel(id: 'c143', segmentId: 's13', key: '613', title: 'Jazz Vocal Blends'),
  Channel(id: 'c144', segmentId: 's13', key: '614', title: 'Classic Rock'),
  Channel(id: 'c145', segmentId: 's13', key: '615', title: 'Reggae'),
  Channel(id: 'c146', segmentId: 's13', key: '616', title: 'Éxitos en Inglés'),
  Channel(id: 'c147', segmentId: 's13', key: '617', title: 'Bailable en Inglés'),
  Channel(id: 'c148', segmentId: 's13', key: '618', title: 'Pop Juvenil'),
  Channel(id: 'c149', segmentId: 's13', key: '619', title: 'Hollywood Hits'),
  Channel(id: 'c150', segmentId: 's13', key: '620', title: 'Latin Jazz'),
  Channel(id: 'c151', segmentId: 's13', key: '621', title: 'Upbeat Oldies'),
  Channel(id: 'c152', segmentId: 's13', key: '622', title: 'Music Pop Brasileira'),
  Channel(id: 'c153', segmentId: 's13', key: '623', title: 'Verde Amarelo'),
  Channel(id: 'c154', segmentId: 's13', key: '624', title: 'Environmental'),
  Channel(id: 'c155', segmentId: 's13', key: '625', title: 'New Age'),
  Channel(id: 'c156', segmentId: 's13', key: '626', title: 'Rock Latino'),
  Channel(id: 'c157', segmentId: 's13', key: '627', title: 'Regional Mexican'),
  Channel(id: 'c158', segmentId: 's13', key: '628', title: 'Power Hits'),
  Channel(id: 'c159', segmentId: 's13', key: '629', title: 'Mariachi'),
  Channel(id: 'c160', segmentId: 's13', key: '630', title: 'Big Band'),
  Channel(id: 'c161', segmentId: 's13', key: '631', title: 'Great Singers'),
  Channel(id: 'c162', segmentId: 's13', key: '632', title: 'World Beat'),
  Channel(id: 'c163', segmentId: 's13', key: '633', title: 'Tango'),
  Channel(id: 'c164', segmentId: 's13', key: '634', title: 'Symphonic'),
  Channel(id: 'c165', segmentId: 's13', key: '635', title: 'Italian Hits'),
  Channel(id: 'c166', segmentId: 's13', key: '636', title: 'Rock n Roll Oldies'),
  Channel(id: 'c167', segmentId: 's13', key: '637', title: 'Caribbean Rhytms'),
  Channel(id: 'c168', segmentId: 's13', key: '638', title: 'Music of Americas'),
  Channel(id: 'c169', segmentId: 's13', key: '639', title: 'El Mocambo'),
  Channel(id: 'c170', segmentId: 's13', key: '640', title: 'Blues'),
  Channel(id: 'c171', segmentId: 's13', key: '641', title: 'Lite Classic'),
  Channel(id: 'c172', segmentId: 's13', key: '642', title: 'Contemporary'),
  Channel(id: 'c173', segmentId: 's13', key: '643', title: 'Hot Jamz'),
  Channel(id: 'c174', segmentId: 's13', key: '644', title: 'Flashback New Wave'),
  Channel(id: 'c175', segmentId: 's13', key: '645', title: 'Bailables en Español'),
  Channel(id: 'c176', segmentId: 's13', key: '646', title: 'Retrodisco'),
  Channel(id: 'c177', segmentId: 's13', key: '647', title: 'Latina'),
  Channel(id: 'c178', segmentId: 's13', key: '648', title: 'Euro Hits'),
  Channel(id: 'c179', segmentId: 's13', key: '649', title: 'Piano'),
  Channel(id: 'c180', segmentId: 's13', key: '650', title: 'Romance Latino')
];
