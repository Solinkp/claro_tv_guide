import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import './screens/segments_screen.dart';
import './screens/segment_channels_screen.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Canales Claro TV',
      theme: ThemeData(
        primarySwatch: Colors.red,
        accentColor: Colors.orange,
        fontFamily: 'Railway',
        textTheme: ThemeData.light().textTheme.copyWith(
          title: TextStyle(fontSize: 18, fontFamily: 'RobotoCondensed', fontWeight: FontWeight.bold, color: Colors.white),
          subhead: TextStyle(fontSize: 20, fontFamily: 'RobotoCondensed', fontWeight: FontWeight.bold, color: Colors.grey),
          subtitle: TextStyle(fontSize: 21, fontFamily: 'RobotoCondensed', fontWeight: FontWeight.bold, color: Colors.blueGrey),
          body1: TextStyle(fontSize: 22, fontFamily: 'RobotoCondensed', fontWeight: FontWeight.bold, fontStyle: FontStyle.italic, color: Colors.blueGrey)
        )
      ),
      home: SegmentsScreen(),
      routes: {
        SegmentChannelsScreen.routeName: (context) => SegmentChannelsScreen()
      },
      debugShowCheckedModeBanner: false,
    );
  }
}