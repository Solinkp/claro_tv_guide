import 'package:flutter/foundation.dart';

class Channel {
  final String id;
  final String segmentId;
  final String key;
  final String title;
  final String hdKey;

  const Channel({ @required this.id, @required this.segmentId, this.key, @required this.title, this.hdKey });
}