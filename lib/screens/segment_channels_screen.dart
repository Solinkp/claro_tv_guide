import 'package:flutter/material.dart';

import '../data.dart';

class SegmentChannelsScreen extends StatelessWidget {
  static const routeName = '/segment-channels';

  @override
  Widget build(BuildContext context) {
    final Map<String, Object> args = ModalRoute.of(context).settings.arguments;
    final String segmentId = args['id'];
    final String segmentTitle = args['title'];
    final Color segmentColor = args['color'];

    final List segmentChannels = CHANNELS_DATA.where((channel) {
      return channel.segmentId == segmentId;
    }).toList();

    return Scaffold(
      appBar: AppBar(title: Text(segmentTitle), backgroundColor: segmentColor,),
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: DataTable(
            horizontalMargin: 10,
            columnSpacing: 10,
            dataRowHeight: 80,
            columns: [
              DataColumn(numeric: true, label: Text('Canal', style: Theme.of(context).textTheme.subhead)),
              DataColumn(numeric: false, label: Text('Nombre', style: Theme.of(context).textTheme.subhead)),
              DataColumn(numeric: true, label: Text('Canal HD', style: Theme.of(context).textTheme.subhead))
            ],
            rows: segmentChannels.map((channel) {
              var key = channel.key != null ? '#${channel.key} :' : '-';
              var hdKey = channel.hdKey != null ? '#${channel.hdKey}' : '-';
              return DataRow(
                cells: [
                  DataCell(Text(key, style: Theme.of(context).textTheme.subtitle,)),
                  DataCell(Text(channel.title, style: Theme.of(context).textTheme.body1)),
                  DataCell(Text(hdKey, style: Theme.of(context).textTheme.subtitle))
                ]
              );
            }).toList()
          ),
        ),
      )
    );
  }
}