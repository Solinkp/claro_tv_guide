import 'package:flutter/material.dart';

import '../widgets/segment_item.dart';
import '../data.dart';

class SegmentsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Claro TV')),
      body: GridView(
        padding: const EdgeInsets.all(15),
        children: SEGMENTS_DATA.map((catData) => SegmentItem(
          catData.id,
          catData.title,
          catData.color
        )).toList(),
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          childAspectRatio: 2,
          crossAxisSpacing: 15,
          mainAxisSpacing: 20
        ),
      ),
    );
  }
}